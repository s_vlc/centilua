centilua
========
The implementation of [Lua][ofc_lua_web] subset for educational purposes.

Features
--------
+ using LL(1) parser
+ implementing subset of lua 5.2
+ implemented using C language (C99 standard)

Licence
-------
GPLv2

[ofc_lua_web]: http://lua.org/  "Official Lua website"
