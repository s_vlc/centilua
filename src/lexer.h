/**
 * @license GPLv2
 * @author slavomir vlcek (2013)
 */

#ifndef LEXER_H
#define LEXER_H

#include "lib/buf.h"
#include "lib/str.h"

enum token_type {
	KW_AND, KW_BREAK, KW_DO, KW_ELSE, KW_ELSEIF, KW_END,
	KW_FALSE, KW_FOR, KW_FUNCTION, KW_GOTO, KW_IF, KW_IN,
	KW_LOCAL, KW_NIL, KW_NOT, KW_OR, KW_REPEAT, KW_RETURN,
	KW_THEN, KW_TRUE, KW_UNTIL, KW_WHILE,
	ID,			/* identifier */
	PLUS_OP,		/* unary or binary plus operator */
	MINUS_OP,		/* unary or binary minus operator */
	MUL_OP,			/* '*' */
	DIV_OP,			/* '/' */
	MOD_OP,			/* '%' */
	POW_OP,			/* '^' */
	LEN_OP,			/* '#' */
	EQ_OP,			/* '==' */
	NEQ_OP,			/* '~=' */
	LTE_OP,			/* '<=' */
	GTE_OP,			/* '>=' */
	LT_OP,			/* '<' */
	GT_OP,			/* '>' */
	ASSIGN_OP,		/* '=' */
	L_PAR,			/* '(' */
	R_PAR,			/* ')' */
	L_CURLY_BRACKET,	/* '{' */
	R_CURLY_BRACKET,	/* '}' */
	L_SQUARE_BRACKET,	/* '[' */
	R_SQUARE_BRACKET,	/* ']' */
	DOUBLE_COLON,
	SEMICOLON,
	COLON,
	COMMA,
	SINGLE_DOT,
	CAT_OP,			/* '..', concatanation operator */
	TRIPLE_DOT,
	STRING,
	NUMBER,
	EOI,			/* end of input */
};

struct lexer {
	struct buf *b;

	int last_ch;
};

union lexeme {
	double d;
	struct str *s;
};

struct token {
	enum token_type tp;
	union lexeme lxme;

	size_t line_no;
	size_t pos_no;
};

void lexer_destruct(void *p);
struct lexer *lexer_construct(char *filepath);
struct token *lexer_token_next(struct lexer *l);

#endif /* LEXER_H */
