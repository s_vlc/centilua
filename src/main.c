#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#include "lexer.h"

int main(int argc, char **argv)
{
	struct lexer *l;
	int ret = EXIT_SUCCESS;
	struct token *tok;

	if (2 != argc) {
		ret = EXIT_FAILURE;
		goto out;
	}
	l = lexer_construct(argv[1]);

	do {
		tok = lexer_token_next(l);
		if (!tok) {
			goto out;
		}
		if (ID == tok->tp || STRING == tok->tp) {
			str_destruct(tok->lxme.s);
		}
	} while (EOI != tok->tp);
out:
	lexer_destruct(l);
        return ret;
}
