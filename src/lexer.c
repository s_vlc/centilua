/**
 * @license GPLv2
 * @author slavomir vlcek (2013)
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "lexer.h"

struct pair {
	char *str;
	enum token_type tp;
};

static struct pair pair_tbl[] = {
	/* keywords */
	{ "and", KW_AND },
	{ "break", KW_BREAK },
	{ "do", KW_DO },
	{ "else", KW_ELSE },
	{ "elseif", KW_ELSEIF },
	{ "end", KW_END },
	{ "false", KW_FALSE },
	{ "for", KW_FOR },
	{ "function", KW_FUNCTION },
	{ "goto", KW_GOTO },
	{ "if", KW_IF },
	{ "in", KW_IN },
	{ "local", KW_LOCAL },
	{ "nil", KW_NIL },
	{ "not", KW_NOT },
	{ "or", KW_OR },
	{ "repeat", KW_REPEAT },
	{ "return", KW_RETURN },
	{ "then", KW_THEN },
	{ "true", KW_TRUE },
	{ "until", KW_UNTIL },
	{ "while", KW_WHILE },
	/*  */
	{ "ID", ID },
	/* operators */
	{ "+", PLUS_OP },
	{ "-", MINUS_OP },
	{ "*", MUL_OP },
	{ "/", DIV_OP },
	{ "%", MOD_OP },
	{ "^", POW_OP },
	{ "#", LEN_OP },
	{ "==", EQ_OP },
	{ "~=", NEQ_OP },
	{ "<=", LTE_OP },
	{ ">=", GTE_OP },
	{ "<", LT_OP },
	{ ">", GT_OP },
	{ "=", ASSIGN_OP },
	{ "(", L_PAR },
	{ ")", R_PAR },
	{ "{", L_CURLY_BRACKET },
	{ "}", R_CURLY_BRACKET },
	{ "[", L_SQUARE_BRACKET },
	{ "]", R_SQUARE_BRACKET },
	{ "..", DOUBLE_COLON },
	{ ";", SEMICOLON },
	{ ":", COLON },
	{ ",", COMMA },
	{ ".", SINGLE_DOT },
	{ "..", CAT_OP },
	{ "...", TRIPLE_DOT },
	/* literals */
	{ "STRING", STRING },
	{ "NUMBER", NUMBER },
	/* end of input */
	{ "EOI", EOI },
};

void lexer_destruct(void *p)
{
	struct lexer *l = (struct lexer *)p;

	if (!l)
		return;
	buf_destruct(l->b);
	free(l);
}

struct lexer *lexer_construct(char *filepath)
{
	struct lexer *l;

	l = (struct lexer *)calloc(1, sizeof(*l));
	if (!l) {
		goto out;
	}
	l->b = buf_construct(filepath);
	if (!l->b) {
		goto out;
	}
	return l;
out:
	lexer_destruct(l);
	return NULL;
}

static enum token_type recognize_keyword(char *str)
{
	int ret;
	/* iterator */
	struct pair *it = pair_tbl;

	while ((ret = strcmp(it->str, str)) < 0 && it->tp <= KW_WHILE)
		++it;
	return 0 == ret ? it->tp : ID;
}

static int get_next_char(struct lexer *l)
{
	return l->last_ch = buf_next(l->b);
}

static void print_token(struct token *tok)
{
	printf("<%s, line: %d, pos: %d, attr: ", pair_tbl[tok->tp].str,
	       (int)tok->line_no, (int)tok->pos_no);
	switch (tok->tp) {
	case ID:
		printf("%s>\n", tok->lxme.s->start);
		break;
	case NUMBER:
		printf("%lf>\n", tok->lxme.d);
		break;
	case STRING:
		printf("\'%s\'>\n", tok->lxme.s->start);
		break;
	default:
		printf("none>\n");
	}
}

/**
 * @note This function cannot be called twice in sequence.
 */
static void pushback_char(struct lexer *l)
{
	buf_pushback(l->b);
}

struct token *lexer_token_next(struct lexer *l)
{
#define IS_KEYWORD(TP) { TP >= KW_AND && TP <= KW_WHILE }

	/* statically allocated variable */
	static struct token tok;
	size_t lexeme_len = 0;
	bool single_quoted_string;
	int ret;
q0:
	ret = get_next_char(l);
	if (-2 == ret) {
		goto err;
	}
	tok.line_no = l->b->cur.line_no;
	tok.pos_no = l->b->cur.pos_no;

	if (isalpha(l->last_ch) || '_' == l->last_ch) {
		tok.lxme.s = str_construct(&(char){l->last_ch}, 1);
		if (!tok.lxme.s) {
			goto err;
		}
		goto q_ident;
	} else if (isdigit(l->last_ch)) {
		tok.tp = NUMBER;
		tok.lxme.d = l->last_ch - (int)'0';
		goto q_number;
	} else if (isspace(l->last_ch)) {
		goto q0;
	} else if (EOF == l->last_ch) {
		tok.tp = EOI;
		goto out;
	} else {
		switch (l->last_ch) {
		case '\'':
		case '"':
			tok.lxme.s = str_construct(NULL, 0);
			if (!tok.lxme.s) {
				goto err;
			}
			single_quoted_string = l->last_ch == '\'';
			tok.tp = STRING;
			goto q_string;
		case '+':
			tok.tp = PLUS_OP;
			goto out;
		case '-':
   			goto q_minus;
		case '*':
			tok.tp = MUL_OP;
			goto out;
		case '/':
			tok.tp = DIV_OP;
			goto out;
		case '%':
			tok.tp = MOD_OP;
			goto out;
		case '^':
			tok.tp = POW_OP;
			goto out;
		case '#':
			tok.tp = LEN_OP;
			goto out;
		case '=':
			goto q_eq;
		case '~':
			goto q_tilde;
		case '<':
			goto q_lt;
		case '>':
			goto q_gt;
		case '(':
			tok.tp = L_PAR;
			goto out;
		case ')':
			tok.tp = R_PAR;
			goto out;
		case '{':
			tok.tp = L_CURLY_BRACKET;
			goto out;
		case '}':
			tok.tp = R_CURLY_BRACKET;
			goto out;
		case '[':
			tok.tp = L_SQUARE_BRACKET;
			goto out;
		case ']':
			tok.tp = R_SQUARE_BRACKET;
			goto out;
		case ',':
			tok.tp = COMMA;
			goto out;
		case '.':
			goto q_dot;
		case ':':
			goto q_colon;
		case ';':
			tok.tp = SEMICOLON;
		default:
			goto err;
		}
	}
q_ident:
	ret = get_next_char(l);
	if (-2 == ret) {
		goto err_str;
	}
	/* identifier may contain numbers, underscores or letters */
	if (isalpha(l->last_ch) || '_' == l->last_ch || isdigit(l->last_ch)) {
		ret = str_append_char(tok.lxme.s, (char)l->last_ch);
		if (0 != ret) {
			goto err_str;
		}
		goto q_ident;
	}
	tok.tp = recognize_keyword(tok.lxme.s->start);
	/* if it is keyword we need to free the string */
	if (ID != tok.tp) {
		str_destruct(tok.lxme.s);
	}
	pushback_char(l);
	goto out;
q_number:
	ret = get_next_char(l);
	if (-2 == ret) {
		goto err;
	}
	if (isdigit(l->last_ch)) {
		tok.lxme.d *= 10;
		tok.lxme.d += l->last_ch - (int)'0';
		goto q_number;
	} else {
		pushback_char(l);
	}
	goto out;

q_string:
	switch (get_next_char(l)) {
	case -2:
		goto err_str;
	case '\'':
		if (single_quoted_string) {
			goto out;
		}
		break;
	case '"':
		if (!single_quoted_string) {
			goto out;
		}
		break;
	case EOF:
		goto err_str;
	}
	ret = str_append_char(tok.lxme.s, l->last_ch);
	if (0 != ret)
		goto err_str;
	++lexeme_len;
	goto q_string;
q_minus:
	switch (get_next_char(l)) {
	case -2:
		goto err;
	case '-':
		goto q_comment;
	default:
		tok.tp = MINUS_OP;
		pushback_char(l);
		break;
	}
	goto out;
q_comment:
	switch (get_next_char(l)) {
	case -2:
		goto err;
	case '[':
		switch (get_next_char(l)) {
		case -2:
			goto err;
		case '[':
			goto q_long_comment;
		}
	default:
		goto q_short_comment;
	}
q_short_comment:
	switch (get_next_char(l)) {
	case -2:
		goto err;
	case '\n':
		goto q0;
	default:
		goto q_short_comment;
	}

q_long_comment:
	switch (get_next_char(l)) {
	case -2:
		goto err;
	case ']':
		switch (get_next_char(l)) {
		case -2:
			goto err;
		case ']':
			goto q0;
		}
	default:
		goto q_long_comment;
	}
q_eq:
	switch (get_next_char(l)) {
	case -2:
		goto err;
	case '=':
		tok.tp = EQ_OP;
		break;
	default:
		tok.tp = ASSIGN_OP;
		pushback_char(l);
		break;
	}
	goto out;
q_tilde:
	switch (get_next_char(l)) {
	case '=':
		tok.tp = NEQ_OP;
		break;
	case -2:
	default:
		goto err;
	}
	goto out;

q_lt:
	switch (get_next_char(l)) {
	case -2:
		goto err;
	case '=':
		tok.tp = LTE_OP;
		break;
	default:
		tok.tp = LT_OP;
		pushback_char(l);
		break;
	}
	goto out;
q_gt:
	switch (get_next_char(l)) {
	case -2:
		goto err;
	case '=':
		tok.tp = GTE_OP;
		break;
	default:
		tok.tp = GT_OP;
		break;
	}
	goto out;
q_dot:
	switch (get_next_char(l)) {
	case -2:
		goto err;
	case '.':
		switch (get_next_char(l)) {
		case -2:
			goto err;
		case '.':
			tok.tp = TRIPLE_DOT;
			break;
		default:
			tok.tp = CAT_OP;
			break;
		}
		break;
	default:
		tok.tp = SINGLE_DOT;
		pushback_char(l);
	}
	goto out;
q_colon:
	switch (get_next_char(l)) {
	case -2:
		goto err;
	case ':':
		tok.tp = DOUBLE_COLON;
		break;
	default:
		tok.tp = COLON;
		pushback_char(l);
	}
	goto out;

out:
	print_token(&tok);
	return &tok;
err_str:
	str_destruct(tok.lxme.s);
err:
	return NULL;
}
