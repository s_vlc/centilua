/**
 * @license GPLv2
 * @author slavomir vlcek (2013)
 */
#include <stdio.h>
#include <stdlib.h>
#include "buf.h"
#include "list.h"
#include "str.h"

#define DEFAULT_BUF_LEN 80

struct pair {
	size_t no;
	struct str *str;
};

static void pair_destruct(void *p)
{
	str_destruct(((struct pair *)p)->str);
	free(p);
}

static int pair_cmp(void *p1, void *p2)
{
	return *(int *)p1 - *(int *)p2;
}

static struct pair *pair_construct(size_t line_no)
{
	struct pair *p;

	p = malloc(sizeof(*p));
	if (p) {
		p->str = str_construct(NULL, 0);
		p->no = line_no;
	}
	return p;
}

/**
 * @note for debugging purposes
 */
void print_line(size_t no, void *data)
{
	struct pair *p = data;

	printf("line_no: %d, str: %s", (int)no + 1, p->str->start);
}

static int buf_reload(struct buf *b)
{
	size_t ret = 0;

	ret = fread(b->start, 1, b->len, b->fp);
	if (0 == ret) {
		if (ferror(b->fp)) {
			fprintf(stderr, "fread() failed.\n");
			ret = -2;
		} else {
			ret = -1;
		}
		fclose(b->fp);
		b->fp = NULL;
	} else {
		b->pos = b->start;
		b->brk = b->start + ret;
		ret = 0;
	}
	return ret;
}

void buf_destruct(void *p)
{
	struct buf *b = (struct buf *)p;

	if (!b)
		return;
	if (b->fp)
		fclose(b->fp);
	list_destruct(b->src_code);
	free(b->start);
	free(b);
}

struct buf *buf_construct(const char *filepath)
{
	int ret;
	struct buf *b = NULL;

	b = (struct buf *)calloc(1, sizeof(*b));
	if (!b) {
		goto out;
	}
	b->start = (char *)calloc(1, DEFAULT_BUF_LEN);
	if (!b->start) {
		goto out;
	}
	b->src_code = list_construct(pair_cmp, pair_destruct);
	if (!b->src_code) {
		goto out;
	}
	if (filepath) {
		b->fp = fopen(filepath, "rt");
		if (!b->fp) {
			fprintf(stderr, "Wrong filepath.\n");
			goto out;
		}
	} else {
		b->fp = stdin;
	}
	b->len = DEFAULT_BUF_LEN;
	ret = buf_reload(b);
	if (-2 == ret) {
		goto out;
	}
	b->cur.value = '\n';
	b->cur.line_no = 0;
	b->cur.pos_no = 0;
	b->pushed_back = false;
	b->last_stored_pos = 0;
	return b;
out:
	buf_destruct(b);
	return NULL;
}

static int append_string(struct list *src_code, size_t line_no,
			 char *from, char *brk)
{
	int ret;
	struct pair *p;
	size_t seq_len = brk - from;

	p = list_get(src_code, &line_no);
	if (!p) {
		p = pair_construct(line_no);
		if (!p) {
			ret = -1;
			goto out;
		}
		ret = list_append(src_code, p);
		if (0 != ret) {
			ret = -1;
			goto out;
		}
	}
	ret = str_append_seq(p->str, from, seq_len);
	if (0 != ret) {
		ret = -1;
	}
out:
	return ret;
}

int buf_next(struct buf *b)
{
	int ret;

	if (!b->fp) {
		ret = EOF;
		goto out;
	}
	if (b->pushed_back) {
		b->pushed_back = false;
		ret = b->cur.value;
		goto out;
	}
	if (b->pos >= b->brk) {
		ret = append_string(b->src_code, b->cur.line_no,
				    b->start + b->last_stored_pos, b->pos);
		if (0 != ret) {
			ret = -2;
			goto out;
		}
		b->last_stored_pos = 0;
		ret = buf_reload(b);
		if (-2 == ret) {
			goto out;
		} else if (-1 == ret) {
			ret = EOF;
			goto out;
		}
	}
	/* if previous character was '\n' */
	if ('\n' == b->cur.value) {
		/* if the line was not empty */
		if (b->cur.pos_no > 0) {
			ret = append_string(b->src_code, b->cur.line_no,
					    b->start + b->last_stored_pos, b->pos);
			if (0 != ret) {
				ret = -2;
				goto out;
			}
			b->last_stored_pos = b->pos - b->start;
		}
		++b->cur.line_no;
		b->cur.pos_no = ('\n' == *b->pos) ? 0 : 1;
	} else {
		++b->cur.pos_no;
	}
	b->cur.value = *b->pos;
	++b->pos;
	ret = b->cur.value;
out:
	return ret;
}

int buf_pushback(struct buf *b)
{
	int ret = -1;

	if (!b->pushed_back) {
		b->pushed_back = true;
		ret = 0;
	}
	return ret;
}
