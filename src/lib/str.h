/**
 * @license GPLv2
 * @author slavomir vlcek (2013)
 */
#ifndef STR_H
#define STR_H

#include <stdio.h>

struct str {
	char *start;
	/* character count without '\0' */
	size_t cnt;
	/* total length */
	size_t len;
};

void str_destruct(void *p);
struct str *str_construct(char *seq, size_t seq_len);
int str_append_char(struct str *s, char c);
int str_append_seq(struct str *s, char *seq, size_t seq_len);
void str_toupper(struct str *s);

#endif /* STR_H */
