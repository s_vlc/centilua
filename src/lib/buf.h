/**
 * @license GPLv2
 * @author slavomir vlcek (2013)
 */
#ifndef BUF_H
#define BUF_H

#include <stdio.h>
#include <stdbool.h>

#define BUF_LEN_MIN 80

struct character {
	int value;
	size_t line_no;
	/* position within a line */
	size_t pos_no;
};

struct buf {
	/* NULL value indicates EOF */
	FILE *fp;

	struct list *src_code;
	/* RENAME IT */
	size_t last_stored_pos;

	struct character cur;
	bool pushed_back;
	char *start;
	/*
	 * valid characters break (range: <b->start, b->start + b->len>),
	 * must not be dereferenced
	 */
	char *brk;
	/* current position */
	char *pos;
	/* total buffer length */
	size_t len;
};

void buf_destruct(void *p);
struct buf *buf_construct(const char *filepath);
int buf_next(struct buf *b);
int buf_pushback(struct buf *b);

#endif /* BUF_H */
