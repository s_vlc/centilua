#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <assert.h>
#include "list.h"

struct item {
	struct item *prev;
	struct item *next;
	void *data;
};

void list_destruct(void *p)
{
	struct list *l = (struct list *)p;
	struct item *cur;

	if (!l)
		return;
	cur = l->head;
	while (cur) {
		struct item *tmp = cur;

		cur = cur->next;
		if (l->free_data_fn)
			l->free_data_fn(tmp->data);
		free(tmp);
	}
	free(l);
}

struct list *list_construct(cmp_key_fn_t cmp_key_fn, free_data_fn_t free_data_fn)
{
	struct list *l = NULL;

	l = (struct list *)calloc(1, sizeof(*l));
	if (!l)
		goto out;
	l->cmp_key_fn = cmp_key_fn;
	l->free_data_fn = free_data_fn;
out:
	return l;
}

/**
 * @brief Get item that greater key than the passed one.
 */
static struct item *list_item_get_equal_or_consequent(struct list *l,
						      void *data,
						      bool *keys_equal)
{
	struct item *cur = l->head;
	/* initially ret must not be 0 (what if "l->head" would be NULL) */
	int ret = 1;

	while (cur && (ret = l->cmp_key_fn(data, cur->data)) > 0) {
		cur = cur->next;
	}
	*keys_equal = !(bool)ret;
	return cur;
}

bool list_key_is_present(struct list *l, void *key)
{
	bool keys_equal;

	list_item_get_equal_or_consequent(l, key, &keys_equal);
	return keys_equal;
}

void *list_get_nth(struct list *l, size_t idx)
{
	void *data;
	struct item *cur = l->head;

	if (idx >= l->cnt) {
		data = NULL;
		goto out;
	}
	while (idx--)
		cur = cur->next;
	data = cur->data;
out:
	return data;
}

/**
 * @brief Get (possibly NULL) pointer to item's data.
 */
void *list_get(struct list *l, void *key)
{
	struct item *wanted;
	bool keys_equal;

	wanted = list_item_get_equal_or_consequent(l, key, &keys_equal);
	return keys_equal ? wanted->data : NULL;
}

/**
 * @brief 
 * @return 0 on success, -1 on "data" presence, -2 on allocation failure
 */
int list_add(struct list *l, void *data)
{
	struct item *tmp;
	bool keys_equal;
	struct item *wanted;
	struct item *prev, *next;
	int ret = 0;

	wanted = list_item_get_equal_or_consequent(l, data, &keys_equal);
	/* if item is already present */
	if (keys_equal) {
		ret = -1;
		goto out;
	}
	tmp = (struct item *)malloc(sizeof(*tmp));
	if (!tmp) {
		ret = -2;
		goto out;
	}
	if (!wanted) {
		/* possibly NULL assignment */
		prev = l->tail;
		next = NULL;
	} else {
		/* possibly NULL assignment */
		prev = wanted->prev;
		next = wanted;
	}
	tmp->prev = prev;
	tmp->next = next;
	if (prev)
		prev->next = tmp;
	else
		l->head = tmp;
	if (next)
		next->prev = tmp;
	else
		l->tail = tmp;
	tmp->data = data;
	++l->cnt;
	ret = 0;
out:
	return ret;
}

int list_append(struct list *l, void *data)
{
	struct item *tmp;
	int ret = 0;

	tmp = (struct item *)calloc(1, sizeof(*tmp));
	if (!tmp) {
		ret = -1;
		goto out;
	}
	if (l->cnt) {
		tmp->prev = l->tail;
		l->tail->next = tmp;
	} else {
		l->head = tmp;
	}
	++l->cnt;
	l->tail = tmp;
	tmp->data = data;
out:
	return ret;
}

void list_print(struct list *l, print_fn_t print_fn)
{
	struct item *cur = l->head;
	size_t no = 0;

	while (cur) {
		print_fn(no++, cur->data);
		cur = cur->next;
	}
}
