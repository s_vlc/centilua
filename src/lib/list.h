/**
 * @brief Generic doubly-linked list.
 * @author S. Vlcek
 * @licence GPLv2
 */
#ifndef LIST_H
#define LIST_H

#include <stdlib.h>
#include <stdbool.h>

typedef int (*cmp_key_fn_t)(void *p1, void *p2);
typedef void (*free_data_fn_t)(void *p);
typedef void (*print_fn_t)(size_t no, void *data);

/**
 * @brief
 */
struct list {
	struct item *head;
	struct item *tail;
	size_t cnt;
	/* comparison function */
	cmp_key_fn_t cmp_key_fn;
	free_data_fn_t free_data_fn;
};

void list_destruct(void *p);
struct list *list_construct(cmp_key_fn_t cmp_key_fn, free_data_fn_t free_data_fn);
bool list_key_is_present(struct list *l, void *key);
void *list_get_nth(struct list *l, size_t idx);
void *list_get(struct list *l, void *key);
int list_add(struct list *l, void *data);
int list_append(struct list *l, void *data);
void list_print(struct list *l, print_fn_t print_fn);

#endif /* LIST_H */
