/**
 * @license GPLv2
 * @author slavomir vlcek (2013)
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "str.h"

#define DEFAULT_STR_LEN 100

void str_destruct(void *p)
{
	struct str *s = (struct str *)p;

	if (!s)
		return;
	free(s->start);
	free(s);
}

struct str *str_construct(char *seq, size_t seq_len)
{
	struct str *s;
	size_t str_len;

	if (seq_len) {
		str_len = seq_len + 1;
	} else {
		str_len = DEFAULT_STR_LEN;
	}
	s = calloc(1, sizeof(*s));
	if (!s) {
		goto err;
	}
	s->start = malloc(str_len);
	if (!s->start) {
		goto err;
	}
	s->cnt = seq_len;
	s->len = str_len;
	/* copy possibly 0 bytes */
	strncpy(s->start, seq, seq_len);
	/* make it string */
	s->start[s->cnt] = '\0';
	return s;
err:
	str_destruct(s);
	return NULL;
}

int str_append_char(struct str *s, char c)
{
	char *tmp_str;
	size_t tmp_len;
	int ret = 0;

	if (s->cnt + 1 >= s->len) {
		tmp_len = s->len + 1 + DEFAULT_STR_LEN;
		tmp_str = realloc(s->start, tmp_len);
		if (!tmp_str) {
			ret = -1;
			goto out;
		}
		s->start = tmp_str;
		s->len = tmp_len;
	}
	s->start[s->cnt++] = c;
	/* make it string */
	s->start[s->cnt] = '\0';
out:
	return ret;
}

int str_append_seq(struct str *s, char *seq, size_t seq_len)
{
	char *tmp_str;
	size_t tmp_len;
	int ret = 0;

	if (s->cnt + seq_len + 1 > s->len) {
		tmp_len = s->len + seq_len + DEFAULT_STR_LEN;
		tmp_str = realloc(s->start, tmp_len);
		if (!tmp_str) {
			ret = -1;
			goto out;
		}
		s->start = tmp_str;
		s->len = tmp_len;
	}
	memcpy(s->start + s->cnt, seq, seq_len);
	s->cnt += seq_len;
	/* make it string */
	s->start[s->cnt] = '\0';
out:
	return ret;
}

void str_toupper(struct str *s)
{
	char *pos = s->start;

	while (*pos) {
		*pos = toupper(*pos);
		++pos;
	}
}
