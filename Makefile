SHELL = /bin/sh

# clear, then define suffix list
.SUFFIXES:
.SUFFIXES: .c .h .o

CC = gcc

# OPTCFLAGS may be passed to "make" to add some extra flags
CFLAGS =-pedantic -Wall -Werror -Wextra -ggdb $(OPTCFLAGS)

# these options are essential, do not let the 'make user' to suppress them
override CFLAGS += -std=c99

# program {source,header,object} files
SRC = $(wildcard src/*.c)
HDR = $(wildcard src/*.h)
OBJ = $(patsubst src/%.c,obj/%.o,$(SRC))

LSRC = $(wildcard src/lib/*.c)
LHDR = $(wildcard src/lib/*.h)
LOBJ = $(patsubst src/lib/%.c,obj/%.o,$(LSRC))

# program target
TARG = centilua

VERSION = 0.1
DISTDIR = $(TARG)-$(VERSION)
# all files important for distribution
DISTFILES = src/ Makefile LICENCE README.md

.PHONY: all clean dist-tgz dist-zip
all   : $(TARG)

$(TARG): $(LOBJ) $(OBJ)
	$(CC) $(CFLAGS) $(LOBJ) $(OBJ) -o $@

# define new rule with higher priority compared to implicit %.c rule
#
# "obj" must be "order-only prerequisite", otherwise "obj"
# causes all source files to be recompiled over and over again
#
# this is not perfect solution -- every source file depends on all header files
# (perfect solution "Generating Prerequisites Automatically" looks scary)
obj/%.o: src/lib/%.c $(LHDR) | obj
	$(CC) $(CFLAGS) -c -o $@ $<

obj/%.o: src/%.c $(HDR) | obj
	$(CC) $(CFLAGS) -c -o $@ $<

# directory creation, no prerequisites there
obj       : ; @mkdir obj
$(DISTDIR): ; @mkdir $(DISTDIR)

# create gzip archive
dist-tgz: $(DISTDIR)
	@cp -r $(DISTFILES) $(DISTDIR)
	tar -czf $(DISTDIR).tgz $(DISTDIR)
	@rm -rf $(DISTDIR)

# create zip archive
dist-zip: $(DISTDIR)
	@cp -r $(DISTFILES) $(DISTDIR)
	zip -r $(DISTDIR).zip $(DISTDIR)
	@rm -rf $(DISTDIR)

clean :
	find . -name "*.o" -type f -print0 | xargs -0 rm -f
	rm -rf obj/ $(TARG)
	rm -f $(DISTDIR).tgz $(DISTDIR).zip
	find . -name "*~" -type f -print0 | xargs -0 rm -f
